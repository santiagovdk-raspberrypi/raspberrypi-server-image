const express = require('express')
const app = express()
const port = 3000

const Gpio = require('onoff').Gpio;
const led = new Gpio(17, 'out');
let ledState = 0;

app.get('/', (req, res) => res.send('Hello World from a RaspberryPi :)'))

app.get('/led', (req, res) => {
    console.log("Led"); 
    if(ledState == 0){
        led.writeSync(1);
    } else {
        led.writeSync(0);
    }  
});

process.on('SIGINT', () => {
    led.unexport();
});

app.listen(port, () => console.log(`Scanner app listening on port ${port}!`))
